package com.example.adapter;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.example.adapter.gridview.GridViewActivity;
import com.example.adapter.listview.CustomListViewActivity;
import com.example.adapter.listview.ListViewActivity;
import com.example.adapter.recyclerView.RecyclerViewActivity;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {
    private Button btnSimpleListView;
    private Button btnCustomAdapterListView;
    private Button btnGridView;
    private Button btnRecyclerView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        btnSimpleListView = findViewById(R.id.btnSimpleListView);
        btnCustomAdapterListView = findViewById(R.id.btnCustomAdapterListView);
        btnGridView = findViewById(R.id.btnGridView);
        btnRecyclerView = findViewById(R.id.btnRecyclerView);

        btnSimpleListView.setOnClickListener(this);
        btnCustomAdapterListView.setOnClickListener(this);
        btnGridView.setOnClickListener(this);
        btnRecyclerView.setOnClickListener(this);

    }

    @Override
    public void onClick(View view) {
        switch (view.getId())
        {
            case R.id.btnSimpleListView:
                startActivity(new Intent(this, ListViewActivity.class));
                break;
            case  R.id.btnCustomAdapterListView:
                startActivity(new Intent(this, CustomListViewActivity.class));
                break;
            case R.id.btnGridView:
                startActivity(new Intent(this, GridViewActivity.class));
                break;
            case  R.id.btnRecyclerView:
                startActivity(new Intent(this, RecyclerViewActivity.class));
                break;
        }
    }
}

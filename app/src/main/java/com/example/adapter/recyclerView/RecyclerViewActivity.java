package com.example.adapter.recyclerView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;

import com.example.adapter.R;
import com.example.adapter.recyclerView.model.Article;

import java.util.ArrayList;
import java.util.List;

public class RecyclerViewActivity extends AppCompatActivity {
    private RecyclerView recyclerView;
    private List<Article> articles = new ArrayList<>();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_recycler_view);

        recyclerView = findViewById(R.id.recyclerview);

        GenerateDummyData();

        RecyclerViewAdapter adapter = new RecyclerViewAdapter();
        adapter.addMoreItem(articles);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setAdapter(adapter);
    }

    private void GenerateDummyData() {
        for(int i=0;i<100;i++){
            this.articles.add(new Article("TITLE"+ i,"DESCRIPTION"+i));
        }
    }
}

package com.example.adapter.recyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.adapter.R;
import com.example.adapter.recyclerView.model.Article;

import java.util.ArrayList;
import java.util.List;

public class RecyclerViewAdapter extends RecyclerView.Adapter<RecyclerViewHolder> {

    List<Article> articleList;

    public RecyclerViewAdapter() {
        articleList = new ArrayList<>();
    }
    public void addMoreItem(List<Article> articles){
        this.articleList.addAll(articles);
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public RecyclerViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        //custom row item
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_recycler_view,parent,false);
        return new RecyclerViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerViewHolder holder, int position) {
        //each item will be bind here;
        holder.onBind(articleList.get(position));
    }

    @Override
    public int getItemCount() {
        return articleList.size();
    }
}

package com.example.adapter.recyclerView;

import android.util.Log;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.adapter.R;
import com.example.adapter.recyclerView.model.Article;

public class RecyclerViewHolder extends RecyclerView.ViewHolder {
    private static final String TAG = RecyclerViewHolder.class.getSimpleName();
    private TextView tvTitle;
    private TextView tvDescription;

    public RecyclerViewHolder(@NonNull View itemView) {
        super(itemView);
        tvTitle = itemView.findViewById(R.id.tvTitle);
        tvDescription = itemView.findViewById(R.id.tvDescription);

        itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Log.e(TAG,"ITEM CLICK");
            }
        });
    }

    public void onBind(Article article) {
        tvTitle.setText(article.getTitle());
        tvDescription.setText(article.getDescription());
    }
}

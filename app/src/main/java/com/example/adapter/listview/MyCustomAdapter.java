package com.example.adapter.listview;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.adapter.R;

public class MyCustomAdapter extends BaseAdapter {
    private Activity context;
    private String[] items;

    class ViewHolder{
        public TextView tvLabel;
        public ImageView ivIcon;

        public ViewHolder(View v){
            tvLabel = v.findViewById(R.id.tvLabel);
            ivIcon = v.findViewById(R.id.ivIcon);
        }
    }

    public MyCustomAdapter(Activity context, String[] items) {
        this.context = context;
        this.items = items;
    }

    @Override
    public int getCount() {
        return items.length;
    }

    @Override
    public Object getItem(int i) {
        return items[i];
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder viewHolder = null;
        //reuse Views;
        if(convertView == null){
            LayoutInflater inflater = context.getLayoutInflater();
            convertView= inflater.inflate(R.layout.item_custom_list_view, parent, false);
            // configure view holder
            viewHolder = new ViewHolder(convertView);  convertView.setTag(viewHolder);
        }
        else
            viewHolder = (ViewHolder) convertView.getTag();
        // fill data
        String s = items[position];  viewHolder.tvLabel.setText(s);
        if (s.startsWith("Windows7") || s.startsWith("iPhone") || s.startsWith("Solaris"))
            viewHolder.ivIcon.setImageResource(R.drawable.ic_clear_black_24dp);
        else
            viewHolder.ivIcon.setImageResource(R.drawable.ic_check_black_24dp);
        return convertView;

    }
}
